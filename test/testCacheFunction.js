
const cacheFunction = require('../cacheFunction.js');

function callback(number)
{
    return number*number;
}

try{
    const result = cacheFunction(callback);

    console.log(result(3));
    console.log(result(5));
    console.log(result(3));
    console.log(result(8));
    console.log(result(8));
    console.log(result(3));

} catch(error){
    console.log(error);
}