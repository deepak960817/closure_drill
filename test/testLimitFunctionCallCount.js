
const limitFunctionCallCount = require('../limitFunctionCallCount.js');

const sum = (a,b) => {
    return a + b;
}

try{
    const result = limitFunctionCallCount(sum, 4);

    console.log(result());
    console.log(result(1));
    console.log(result(1,2));
    console.log(result(1,2,3));
    console.log(result());

} catch(error){
    console.log(error);
}



