
function counterFactory() {

    counterVariable = 15;

    function increment() 
    {
        counterVariable++;
        return counterVariable;
    }
    function decrement() 
    {   
        let counterVariable = 21;
        return --counterVariable;
    }
    return {"Incremented Value" : increment(), "Decremented Value" : decrement() };
}

module.exports = counterFactory;