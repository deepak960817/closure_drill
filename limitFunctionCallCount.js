
function limitFunctionCallCount(cb, n) {
    
    if(typeof cb === 'undefined' || typeof n === 'undefined')
    {
        throw new Error("Parameters not passed correctly");
    }
    let executionsCount = 0;
    function invoke()
    {
        executionsCount++;
        if(executionsCount <= n)
        {
            //console.log(executionsCount);
            return cb(4,6);
        }
        else
        {
            return null;
        }
    }
    return invoke;
}

module.exports = limitFunctionCallCount;