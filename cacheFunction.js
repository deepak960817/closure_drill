
function cacheFunction(cb) {

    if(typeof cb !== 'function')
    {
        throw new Error("Parameters not passed correctly");
    }
    let cache = {};
    return function invoke(input)
    {
        if(input in cache)
        {
            console.log("Looking in cache");
            return cache[input];
        }
        else
        {
            //console.log(cache[input])
            let output = cb(input);
            cache[input] = output;
            return output; 
        }
    }
}

module.exports = cacheFunction;